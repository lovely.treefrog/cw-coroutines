/*
  Copyright (c) 2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Kotlin Coroutines_

  https://commonsware.com/Coroutines
*/

package com.commonsware.coroutines.location

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.android.asCoroutineDispatcher
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch

private const val PERM = Manifest.permission.ACCESS_FINE_LOCATION

class MainMotor(
  private val locationManager: LocationManager,
  private val context: Context
) : ViewModel() {
  private val _results = ConflatedBroadcastChannel<Location>()
  val results = _results.asFlow()
  private val _errors = Channel<Throwable>()
  val errors = _errors.receiveAsFlow()
  private val _permissions = Channel<List<String>>()
  val permissions = _permissions.receiveAsFlow()
  private val handlerThread = HandlerThread("WhereWeAt").apply { start() }
  private val handler = Handler(handlerThread.looper)
  private val dispatcher = handler.asCoroutineDispatcher()
  private var hasLoaded = false

  @SuppressLint("MissingPermission")
  fun loadLocation(force: Boolean = false) {
    if (!hasLoaded || force) {

      if (ContextCompat.checkSelfPermission(context, PERM) == PackageManager.PERMISSION_GRANTED) {
        hasLoaded = true

        viewModelScope.launch(dispatcher) {
          val listener = object : LocationListener {
            override fun onLocationChanged(location: Location) {
              _results.offer(location)
            }

            override fun onStatusChanged(
              provider: String,
              status: Int,
              extras: Bundle?
            ) {
              // unused
            }

            override fun onProviderEnabled(provider: String?) {
              // unused
            }

            override fun onProviderDisabled(provider: String?) {
              // unused
            }
          }

          try {
            locationManager.requestSingleUpdate(
              LocationManager.GPS_PROVIDER,
              listener,
              null
            )
          } catch (t: Throwable) {
            _errors.offer(t)
          }
        }
      } else {
        _permissions.offer(listOf(PERM))
      }
    }
  }

  override fun onCleared() {
    super.onCleared()

    handlerThread.quitSafely()
  }
}